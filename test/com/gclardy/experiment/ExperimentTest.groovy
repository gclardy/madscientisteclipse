package com.gclardy.experiment
import com.gclardy.experiment.DummySupplier
import com.gclardy.experiment.Experiment
import com.gclardy.experiment.testObjects.TestObject1
import com.gclardy.experiment.testObjects.TestObject2
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by tso6716 on 1/10/2017.
 */
public class ExperimentTest extends Specification
{
    @Unroll
    def "testing experiment.run()" ()
    {
        given:
            Experiment<String> experiment = new Experiment<>("test")
            DummySupplier<String> supplier1 = new TestObject1(name: controlName, number: controlNum)
            DummySupplier<String> supplier2 = new TestObject2(name: candidateName, number: candidateNum)

        when:
            String experimentResult = experiment.run(supplier1, supplier2)

        then:

            assert experimentResult == stringResult

        where:
            controlName | candidateName | controlNum | candidateNum | stringResult
            "control"   | "candidate"   | 1          | 1            | "control1"
    }
}
