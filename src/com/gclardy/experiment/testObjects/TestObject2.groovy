package com.gclardy.experiment.testObjects

import com.gclardy.experiment.DummySupplier

/**
 * Created by tso6716 on 1/11/2017.
 */
class TestObject2 implements DummySupplier<String>
{
    String name
    int number

    public String addNumberToName()
    {
        return new String(name+number)
    }

    public String get()
    {
        return addNumberToName()
    }
}
