package com.gclardy.experiment
/**
 * Created by Garth on 1/6/2017.
 */
public interface DummySupplier<T>
{
    T get();
}
