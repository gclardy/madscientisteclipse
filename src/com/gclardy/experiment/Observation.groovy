package com.gclardy.experiment
/**
 * Created by tso6716 on 1/9/2017.
 */
public class Observation<T>
{
    private String name
    private T value
    private Exception exception

    public String getName()
    {
        return name
    }

    public void setName(String name)
    {
        this.name = name
    }

    public T getValue()
    {
        return value
    }

    public void setValue(T value)
    {
        this.value = value
    }

    public void setException(Exception exception)
    {
        this.exception = exception
    }

    public Exception getException()
    {
        return exception
    }
}
