package com.gclardy.experiment

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Garth on 1/6/2017.
 */
public class Experiment<T>
{
    ExecutorService executor = Executors.newFixedThreadPool(2);
    private final String name;

    public Experiment(String name)
    {
        this.name = name;
    }

    public T run(DummySupplier<T> control, DummySupplier<T> candidate)
    {
        boolean isDiff = true;
        Future<Observation<T>> futureCandidate = null
        Future<Observation<T>> futureControl = null

        futureControl = executor.submit(executeResult("control", control, true))
        futureCandidate = executor.submit(executeResult("candidate", candidate, false))

        isDiff = compare(futureCandidate, futureCandidate)

        return futureControl.get().getValue()
    }

    public boolean compare(Future<Observation<T>> futureCandidate, Future<Observation<T>> futureControl) {
        return futureControl.get().getValue().equals(futureCandidate.get().getValue())
    }

    public Callable<Observation<T>> executeResult(String name, DummySupplier<T> control, boolean throwException)
    {
        Observation<T> observation = new Observation<T>(name:name)
        ObservationCallable<T> callable = new ObservationCallable<T>()

        try
        {
            observation.setValue(control.get())
        }
        catch (Exception e)
        {
            observation.setException(e)
        }
        finally
        {
            if(throwException && observation?.getException() != null)
            {
                throw observation.getException()
            }

            callable.setObservation(observation)

            return callable
        }
    }



}
