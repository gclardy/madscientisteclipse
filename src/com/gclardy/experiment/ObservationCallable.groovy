package com.gclardy.experiment

import java.util.concurrent.Callable;

/**
 * Created by tso6716 on 1/10/2017.
 */
public class ObservationCallable<T> implements Callable<Observation<T>>
{
    private Observation<T> observation

    public Observation<T> getObservation()
    {
        return observation
    }

    public void setObservation(Observation<T> observation)
    {
        this.observation = observation
    }

    @Override
    public Observation<T> call() throws Exception {
        return observation
    }
}
